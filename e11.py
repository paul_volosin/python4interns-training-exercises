# Ex. 11
# Develop a python program that does the following on your MacOS:
# - creates a txt file;
# - adds some lines in it;
# - appends some new text to it;
# - prints each line;
# - removes newline characters from file;
# - prints the file;
# - deletes the file;


import os


def solution(filename):
    with open(filename, 'x') as f:
        f.write('\n\n\n\n\n')
        f.write('Some Text')

    with open(filename, 'r') as f:
        for line in f:
            print(line)

    with open(filename, '+') as f:
        flines = f.readlines()
        flines_without_nl = [line.replace('\n','') for line in flines]
        f.writelines(flines_without_nl)

