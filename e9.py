# Ex. 9
# Write a Python program to map two lists into a dictionary


def map_2_lists_into_dict(list_1, list_2):
    return dict(zip(list_1, list_2))


if __name__ == '__main__':
    input_list1 = input('Enter elements of List #1 separated by space:').split()
    input_list2 = input('Enter elements of List #2 separated by space:').split()
    print(map_2_lists_into_dict(input_list1, input_list2))
