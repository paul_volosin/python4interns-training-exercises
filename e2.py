# Ex. 1
# Write a Python program to reverse two strings if length is > 10. If not, concatenate those strings and print them uppercase


def rev_if_len_else_concatenate(str1, str2):
    if len(str1)>10 and len(str2)>10:
        print('The reversed strings are: {} and {}'.format(str1[::-1],str2[::-1]))
    else:
        print((str1+str2).upper())


if __name__=='__main__':
    s1, s2 = input('Enter String #1:'), input('Enter String #2:')
    rev_if_len_else_concatenate(s1, s2)
