# Ex. 4
# Write a function that finds all the prime numbers lower than a given number

from e3 import is_prime


def determine_lower_primes(number):
    lower_primes = [lo_number for lo_number in range(2, number) if is_prime(lo_number)]
    return lower_primes


if __name__ == '__main__':
    no = int(input('Enter a number:'))
    lower_prime_numbers = determine_lower_primes(no)
    print(lower_prime_numbers)
