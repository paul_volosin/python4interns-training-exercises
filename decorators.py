def add_tag(tag):
    def dec_f(old_f):
        def new_f(*args, **kwargs):
            print('<{0}>{1}</{0}>'.format(tag, old_f(*args)))
        return new_f
    return dec_f


@add_tag('p')
def write_paragraphs(what):
    return what


@add_tag('h1')
def write_headings(what):
    return what


if __name__ == '__main__':
    write_paragraphs('hi')
    write_headings('hi')
