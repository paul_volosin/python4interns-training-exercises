# Ex. 1
# Write a Python program to calculate the length of a string and count the number of characters (character frequency) in that string.


def calcStringLengthAndCharFrequency(string):
    string_length = len(string)
    string_characters = {}
    for c in string:
        if c in string_characters.keys():
            string_characters[c] += 1
        else:
            string_characters[c] = 1
    print('#' * 75, "\nThe string's length is {}".format(string_length))
    print('#' * 75, '\nThe character frequency for each character in the string is:')
    for key in string_characters.keys():
        print('Character {}: {}'.format(key, string_characters[key]))


if __name__ == "__main__":
    s = input("Enter a String:")
    calcStringLengthAndCharFrequency(s)
