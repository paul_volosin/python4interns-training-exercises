# Ex. 10
# Write a Python program to check if a dict is empty or not.
# If it's empty, create a dict from a string where: <key> = letter and <value> is the counter of that letter.
# If it's not empty, merge those two dictionaries.


def calc_char_frequency(string):
    freq = {}
    for c in string:
        if c in freq.keys():
            freq[c] += 1
        else:
            freq[c] = 1
    return freq


def solution(dict_1, str_1):
    if not dict_1:
        dict_from_str = calc_char_frequency(str_1)
    else:
        pass
