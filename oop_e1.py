# OOP Ex. 1
# Implement a singleton type of class (which can only have one instance)


class Singleton:
    imp_prop = 'Something to be available to all instances of this class'
    _instance = None

    def __new__(cls):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def set_imp_prop(self, prop):
        self.imp_prop = prop

    def get_imp_prop(self):
        return self.imp_prop


if __name__ == '__main__':
    singleton = Singleton()
    singletone = Singleton()
    print(singleton.get_imp_prop())
    print(singletone.get_imp_prop())
    singleton.set_imp_prop('Very important stuff')
    print(singleton.get_imp_prop())
    print(singletone.get_imp_prop())
    singletone.set_imp_prop('Such very important stuff')
    print(singleton.get_imp_prop())
    print(singletone.get_imp_prop())
