# Ex. 7
# Write a Python program to add an item in a tuple and convert that tuple to a string


def convert_tuple_to_string(tup):
    """
    # Alternative (using functools.reduce() and operator.add):
    import functools
    import operator
    tup_string = functools.reduce(operator.add, (tup))
    return tup_string
    """
    tup_string = ' '.join(tup)
    return tup_string


def add_to_tuple_and_convert_to_string(tuple_1, element_to_add):
    tuple_1 = tuple_1 + (element_to_add, )
    return convert_tuple_to_string(tuple_1)


if __name__ == '__main__':
    input_tuple = input("Enter the tuple's elements separated by space:").split()
    input_element = input('Enter the element to be added to the tuple:')
    tuple_r = tuple(input_tuple)
    tuple_string = add_to_tuple_and_convert_to_string(tuple_r, input_element)
    print('The resulting string is:\n{}'.format(tuple_string))
