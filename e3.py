# Ex. 3
# Write a function that reads a number from the console and checks if it's prime


def is_prime(number):
    if number < 1:
        return False
    elif number == 2:
        return True
    elif number % 2 == 0:
        return False
    else:
        for i in range(3, int(number**(1/2))+1, 2):
            if number % i == 0:
                return False
    return True


if __name__ == '__main__':
    no = int(input('Enter a number:'))
    if is_prime(no):
        print('The entered number is prime.')
    else:
        print('The entered number is not prime.')