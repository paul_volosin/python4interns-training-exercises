# Ex. 2 from the list
# Use the dummy API and, with list comprehension, filter out all employees with age < 30
# Dummy API: http://dummy.restapiexample.com/api/v1/employees
import requests


def filter_employees_by_age_lt(emp_list):
    result_list = [emp['employee_name'] for emp in emp_list if int(emp['employee_age']) < 30]
    return result_list


def gather_employees(api_url, req_head):
    response = requests.get(api_url, headers=req_head)
    return response.json()


if __name__ == '__main__':
    req_header = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    emps = gather_employees('http://dummy.restapiexample.com/api/v1/employees', req_header)
    print(filter_employees_by_age_lt(emps))
