# Ex. 6
# Write a Python program to get the 3rd and 4th from last element


def get_3rd_and_4th_from_last_elements_of_tuple(tuple_1):
    return tuple_1[-3], tuple_1[-4]


if __name__ == '__main__':
    input_tuple = input("Enter the tuple's elements separated by space:").split()
    tuple_1 = tuple(input_tuple)
    # tuple_1_int = tuple(int(el) for el in tuple_1)
    tuple_1_3rd_el, tuple_1_4th_el = get_3rd_and_4th_from_last_elements_of_tuple(tuple_1)
    # tuple_1_int_3rd_el, tuple_1_int_4th_el = get_3rd_and_4th_from_last_elements_of_tuple(tuple_1_int)
    print("The tuple's 3rd and 4th from last (string) elements are: {}, {}".format(tuple_1_3rd_el,tuple_1_4th_el))
    # print("The tuple's 3rd and 4th from last (integer) elements are: {}, {}".format(tuple_1_int_3rd_el, tuple_1_int_4th_el))
