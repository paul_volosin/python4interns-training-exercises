# Ex. 5
# Given two lists create a set by picking odd-index elements from the first list and even index elements from the second


def crossover_merge_lists(list_1, list_2):
    concatenated_list = list_1[1::2] + list_2[::2]
    result_set = set(concatenated_list)
    return result_set


if __name__ == '__main__':
    input_list1 = input('Enter elements of List #1 separated by space:').split()
    input_list2 = input('Enter elements of List #2 separated by space:').split()
    li1 = [int(el) for el in input_list1]
    li2 = [int(el) for el in input_list2]
    print(crossover_merge_lists(li1, li2))
