# Ex. 4 out of the second list
# Write a sum function that takes an arbitrary number of positional arguments and returns the total


def sum_args(*args):
    s = 0
    for arg in args:
        s += arg
    return s


if __name__ == '__main__':
    l = [int(el) for el in input('Enter the args separated by space:').split(' ')]
    print('The sum of the given arguments is: {}'.format(sum_args(*l)))
