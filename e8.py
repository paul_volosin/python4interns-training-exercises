# Ex. 8
# Write a Python program to check if a given key already exists in a dictionary
import ast


def check_if_key_exists_in_dict(dict_1, key_1):
    return key_1 in dict_1


if __name__ == '__main__':
    # Read a dict from input
    s_inp = input('Enter a dictionary: ')
    d_inp = ast.literal_eval(s_inp)
    # Read the key to be checked if it exists in dict
    k_inp = input('Enter the key to be checked if it exists in dict: ')
    print(check_if_key_exists_in_dict(d_inp, k_inp))
