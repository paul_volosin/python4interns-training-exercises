# OOP Ex. 2 (#3 from the second list)
# Make an item class that allows the following operations:
# bread = Item(5)
# water = Item(2)
# mustard = Item(4)
# print(bread + water)
# print(bread + water + mustard)


class Item:
    """
    class Item
    """
    def __init__(self, quantity = 0):
        self.quantity = quantity

    def __repr__(self):
        return 'Class Item({})'.format(self.quantity)

    def __add__(self, other):
        if isinstance(other, type(self)):
            return Item(self.quantity + other.quantity)
        else:
            return TypeError('The two object are not of the same type')


if __name__ == '__main__':
    bread = Item(5)
    water = Item(2)
    mustard = Item(4)
    print(bread + water)
    print(bread + water + mustard)
